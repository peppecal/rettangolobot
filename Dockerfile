FROM python:slim

RUN pip install --upgrade pip python-telegram-bot

ADD app /app
WORKDIR /
CMD ["python", "-m", "app"]
