from telegram.ext import Updater
import os
token = os.getenv('TOKEN')
updater = Updater(token=token, use_context=True)
dispatcher = updater.dispatcher

import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)


from .lib import rettangolo
dispatcher.add_handler(rettangolo.conversation)


if __name__ == "__main__":
    updater.start_polling()
