from telegram.ext import CommandHandler, ConversationHandler, MessageHandler
from telegram.ext import Filters

BASE, ALTEZZA, RISULTATO = range(3)

def start(update, context):
    update.message.reply_text(
        "Ciao! Mi chiamo Rettangolo e sono un bot dedicato a calcolare aree e perimetri dei rettangoli!"
        )

#     return BASE

# def base(update, context):
    update.message.reply_text(
        "Qual è la base del tuo rettangolo?"
        )

    return ALTEZZA

def altezza(update, context):
    context.user_data['base'] = int(update.message.text)
    
    update.message.reply_text(
        "Qual è l'altezza del tuo rettangolo?"
        )

    return RISULTATO

def risultato(update, context):
    context.user_data['altezza'] = int(update.message.text)

    base=context.user_data['base']
    altezza=context.user_data['altezza']
    
    update.message.reply_text(
        """Ricapitolando: il tuo rettangolo ha base {base} ed altezza {altezza}.

Pertanto, si può dire che se gli assiomi euclidei valgono il tuo rettangolo ha area {area} e perimetro {perimetro}, e che inoltre {non}sia un quadrato.
        """.format(
            base=base,
            altezza=altezza,
            area=base*altezza,
            perimetro=2*(base+altezza),
            non="" if (base == altezza) else "non "
            )
        )
    return ConversationHandler.END

def error(update, context):
    update.message.reply_text(
        "Non ho ben capito, vuoi ricominciare?\n\nInvoca pure /start"
    )

conversation=ConversationHandler(
    entry_points=[CommandHandler('start', start)],
    states={
        #BASE: [MessageHandler(Filters.text, base)],
        ALTEZZA: [MessageHandler(Filters.text, altezza)],
        RISULTATO: [MessageHandler(Filters.text, risultato)]
        },
    fallbacks = [MessageHandler(Filters.all, error)]
)
